<?php

Route::group(array('prefix' => 'admin', 'before' => 'admin'), function(){
    Route::resource('users', 'AdminUsersController', array('except' => array('show')));
    Route::resource('contents', 'AdminContentsController', array('except' => array('show','index')));

    Route::resource('assessments', 'AdminAssessmentsController', array('except' => array('show')));

    Route::resource('medicals', 'AdminMedicalsController', array('except' => array('show')));

    Route::resource('schedules', 'AdminSchedulesController', array('except' => array('show','edit','update')));

	Route::get('news', array('as' => 'admin.news', 'uses' => 'AdminContentsController@getNews'));
    Route::get('tips', array('as' => 'admin.tips', 'uses' => 'AdminContentsController@getTips'));

    Route::get('users/assesslist', array('as' => 'admin.getAssessList', 'uses' => 'AdminUsersController@getAssessList'));

    //Route::get('users/addassessmentperuser', array('as' => 'admin.addassessperuser', 'uses' => 'AdminUsersController@addassessmentperuser'));



    Route::get('users/adduserassesments', array('as' => 'admin.getNewAsess', 'uses' => 'AdminUsersController@getNewAsess'));
    Route::post('users/adduserassesments', array('as' => 'admin.postNewAsess', 'uses' => 'AdminUsersController@postNewAsess'));


    Route::get('users/edituassess', array('as' => 'admin.editAsess', 'uses' => 'AdminUsersController@geteditAsessbyDate'));
    Route::post('users/edituassess', array('as' => 'admin.editAsess', 'uses' => 'AdminUsersController@posteditAsessbyDate'));

    Route::post('users/deluassess', array('as' => 'admin.delAsess', 'uses' => 'AdminUsersController@delAsessbyDate'));

    //Route::resource('userassessment', 'AdminUsersAssessmentsController', array('except' => array('show')));



});


Route::group(array('prefix' => 'admin'), function(){

    Route::post('users/addassessmentperuser', array('as' => 'admin.addassessperuser', 'uses' => 'AdminUsersController@addassessmentperuser'));

    Route::get('login', array( 'as' => 'admin.login',  'uses' => 'AdminAuthController@getLogin'));
    Route::post('login', array('as' => 'admin.login.post', 'uses' => 'AdminAuthController@postLogin'));
    Route::get('logout', array( 'as' => 'admin.logout', 'uses' => 'AdminAuthController@getLogout'));


});


Route::group(array('prefix' => 'service'), function(){

    Route::get('login', array( 'uses' => 'ServiceAuthController@getLogin'));
    Route::post('login', array( 'as' => 'service.login.post','uses' => 'ServiceAuthController@postLogin'));
    
    Route::get('users/medicallist', array( 'uses' => 'ServiceUsersController@medicallist'));

    Route::post('register', array( 'as' => 'service.login.post','uses' => 'ServiceAuthController@postRegister'));



    //Route::get('news/findtips', array( 'uses' => 'ServiceUsersController@findtips'));

    //Route::post('news/findtips', array( 'uses' => 'ServiceUsersController@findtips'));


   

    

});

Route::group(array('prefix' => 'service' , 'before' => 'service'), function(){

    Route::get('news/display', array( 'uses' => 'ServiceUsersController@findnews'));

    Route::get('news/details/{news_id}', array( 'uses' => 'ServiceUsersController@newsdetails'));

    Route::get('news/findtips', array( 'uses' => 'ServiceUsersController@findtips'));
   

    Route::get('schedules/getreschedule/{userName}', array( 'uses' => 'ServiceSchedulesController@getreschedule'));

   Route::get('logout', array( 'uses' => 'ServiceAuthController@getLogout'));

   Route::get('users/assessmentlist', array( 'uses' => 'ServiceUsersController@assessmentlist'));

   Route::get('users/assessmentcurvedata/{assessmentId}/{userId}', array( 'uses' => 'ServiceUsersController@assessmentcurvedata'));

   Route::get('users/profilemedical', array( 'uses' => 'ServiceUsersController@profilemedical'));

   Route::post('users/updateprofile', array( 'uses' => 'ServiceUsersController@updateprofile'));




});

// Route::group(array('prefix' => 'admin', 'before' => 'auth'), function(){
//     Route::resource('posts', 'AdminPostsController', array('except' => array('show')));
// });

// Route::get('/', array('as' => 'home', 'uses' => 'PostsController@getIndex'));
// Route::get('post/{id}', array('as' => 'post', 'uses' => 'PostsController@getPost'))->where('id', '[1-9][0-9]*');
