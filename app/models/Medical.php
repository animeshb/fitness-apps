<?php

class Medical extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['name'];

	public function user() {

		return $this->belongsToMany('User');	
	}

	public static function allPaginate($perPage=5){

		 $users = Medical::orderBy('created_at', 'DESC')
		 				->paginate($perPage);

        return $users;


	}

}