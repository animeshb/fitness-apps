<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	 public static $rules = [
        'username' => 'required|email',
        'password' => 'required'
    ];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	// Don't forget to fill this array
	protected $fillable = ['username', 'password', 'token'];

	 public function profile()
    {
        return $this->hasOne('Profile');
    }

    public function medical() {

		return $this->belongsToMany('Medical');	
	}


	public function assessments() {

		return $this->belongsToMany('Assessment')->withPivot('assessment','assessment_date','assessment_time');	
	}

	
	public static function allPaginate($perPage=5){

		 $users = User::where('is_admin', '!=', 1)
		 				->orderBy('created_at', 'DESC')
		 				->paginate($perPage);

        return $users;


	}


	/**
	 * Check user's permissions
	 *
	 * @return bool
	 */
	public function isAdmin()
	{
		return ($this->is_admin == true);
	}

}
