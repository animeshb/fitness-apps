<?php

class Assessment extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'name' => 'required',
		'unit' => 'required',
	];

	// Don't forget to fill this array
	protected $fillable = ['name','unit'];

	public function users() {

		return $this->belongsToMany('User')->withPivot('assessment','assessment_date','assessment_time');	
	}

	public static function allPaginate($perPage=5){

		 $users = Assessment::orderBy('created_at', 'DESC')
		 				->paginate($perPage);

        return $users;


	}


}