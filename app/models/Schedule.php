<?php

class Schedule extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
							'prev_date' => 'required',
							'schedule_date' => 'required',
							'place' => 'required'
							
	];

	// Don't forget to fill this array
	protected $fillable = ['prev_date',
							'prev_time',
							'schedule_date',
							'schedule_time',
							'place',
							'sent_to'
							];

	public static function allPaginate($perPage=5){

		 $schedules = Schedule::orderBy('created_at', 'DESC')
		 				->paginate($perPage);

        return $schedules;


	}

}