<?php

class Content extends \Eloquent {

	// Add your validation rules here
	public static $rules = [
		'title' => 'required',
		'body' => 'required',
		'url' => 'required',
		'type' => 'required'
	];

	// Don't forget to fill this array
	protected $fillable = ['title','body','url','type'];

	public static function newsPaginate($tips =1,$perPage=5){

		 $news = Content::where('type', '=', $tips)
		 				->orderBy('created_at', 'DESC')
		 				->paginate($perPage);

        return $news;


	}

	


}