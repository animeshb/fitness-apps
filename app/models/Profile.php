<?php

class Profile extends \Eloquent {
	

	protected $fillable = [
							'first_name',
							'last_name',
							'user_id',
							'gender',
							'birth_date',
							'address1',
							'phone',
							'mobile',
							'address2',
							'city',
							'postcode',
							'state',
							'country'
						];

	public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required'
    ];

	 public function user()
    {
        return $this->belongsTo('User');
    }
}