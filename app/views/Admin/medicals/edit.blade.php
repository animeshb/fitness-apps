@extends('_layouts.default')

@section('content')
<div class="users form">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Edit Assessment</h1>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">

								<li>
									{{ Form::open(array('route' => array('admin.medicals.destroy', $medical->id), 'method' => 'delete', 'class' => 'destroy')) }}
						            <span class="glyphicon glyphicon-remove"></span>
						            {{ Form::submit('DELETE') }}
						            {{ Form::close() }}
								</li>

								<li>
									<a href="{{route('admin.medicals.index')}}">
										<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Medicals</a>

								</li>					
							</ul>
						</div>
					</div>
				</div>			
		</div>{{-- end col md 3 --}}
		<div class="col-md-9">
			<div>
				 @if(Session::get('errors'))
                        <div class="alert alert-danger alert-dismissable">
                             @foreach($errors->all('<p>:message</p>') as $message)
                                {{$message}}
                             @endforeach
                        </div>
                    @endif
			</div>
    		{{ Form::model($medical, array('route' => array('admin.medicals.update', $medical->id), 'method' => 'put')) }}
			    @include('admin.medicals._partials.form')
			{{ Form::close() }}
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}
</div>
@stop

