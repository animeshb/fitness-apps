@extends('_layouts.default')

@section('content')

<div class="users index">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Assessment List</h1>
			</div>
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="{{route('admin.assessments.create')}}">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Assessment</a>
								</li>
							</ul>
						</div>{{-- end body --}}
				</div>{{-- end panel --}}
			</div>{{-- end actions --}}
		</div>{{-- end col md 3 --}}

		<div class="col-md-9">
			<table cellpadding="0" cellspacing="0" class="table table-striped">
			<thead>
				<tr>
					<th>Name</th>
					<th>Unit</th>
					<th class="actions">Actions</th>
				</tr>
			</thead>
			<tbody>

	@foreach($assessments as $assessment)
	<tr>
		<td>{{$assessment->name}}</td>
		<td>{{$assessment->unit}}</td>
		<td class="actions">
			<a href="{{route('admin.assessments.edit', array($assessment->id))}}"><span class="glyphicon glyphicon-edit"></span></a>
			{{ Form::open(array('route' => array('admin.assessments.destroy', $assessment->id), 'method' => 'delete', 'class' => 'destroy')) }}
            {{ Form::submit('X' , array('class' => "glyphicon glyphicon-remove")) }}
            {{ Form::close() }}
		</td>
	</tr>
	@endforeach
	</tbody>
</table>

			
		</div> {{-- end col md 9 --}}
	</div>{{-- end row --}}

	<div>{{ $assessments->links() }}</div>
</div>{{-- end containing of content --}}
@stop