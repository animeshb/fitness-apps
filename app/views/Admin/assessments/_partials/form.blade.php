<div class="form-group">
	{{ Form::label('name', 'Name') }}
    {{ Form::text('name', null ,array('class' => 'form-control', 'placeholder' => 'Name') ) }}

</div>
<div class="form-group">
	{{ Form::label('unit', 'Unit') }}
    {{ Form::number('unit' ,  null , array('class' => 'form-control', 'placeholder' => 'Unit') ) }}
</div>
<div class="form-group">
	 {{ Form::submit('Save' , array('class' => 'btn btn-default')) }}
</div>
