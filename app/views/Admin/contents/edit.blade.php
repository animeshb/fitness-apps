@extends('_layouts.default')

@section('content')
<div class="users form">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				@if($content->type==1)
				{{{$type = 'News'}}}
				@else
				{{{$type = 'Tips'}}}
				@endif
				<h1>{{'Edit '.$type}}</h1>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">

								<li>
									{{ Form::open(array('route' => array('admin.contents.destroy', $content->id), 'method' => 'delete', 'class' => 'destroy')) }}
						            <span class="glyphicon glyphicon-remove"></span>
						            {{ Form::submit('DELETE') }}
						            {{ Form::close() }}
								</li>

								<li>
									<a href="{{URL::previous()}}">
										<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List</a>
								</li>					
							</ul>
						</div>
					</div>
				</div>			
		</div>{{-- end col md 3 --}}
		<div class="col-md-9">
			<div>
				 @if(Session::get('errors'))
                        <div class="alert alert-danger alert-dismissable">
                             @foreach($errors->all('<p>:message</p>') as $message)
                                {{$message}}
                             @endforeach
                        </div>
                    @endif
			</div>
    		{{ Form::model($content, array('route' => array('admin.contents.update', $content->id), 'method' => 'put')) }}
			    @include('admin.contents._partials.form')
			{{ Form::close() }}
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}
</div>
@stop

