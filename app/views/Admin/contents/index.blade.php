@extends('_layouts.default')

@section('content')

<div class="users index">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				@if($type==1)
				{{{$type = 'News'}}}
				@else
				{{{$type = 'Tips'}}}
				@endif
				<h1>{{ 'ALL '.$type }}</h1>
			</div>
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="{{route('admin.contents.create')}}">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;{{'New '.$type}} </a>
								</li>
							</ul>
						</div>{{-- end body --}}
				</div>{{-- end panel --}}
			</div>{{-- end actions --}}
		</div>{{-- end col md 3 --}}

		<div class="col-md-9">
			<table cellpadding="0" cellspacing="0" class="table table-striped">
				<thead>
					<tr>
			<th>Title</th>
			<th>Body</th>
			<th>Url</th>
			<th class="actions">Actions</th>
	</tr>
	</thead>
	<tbody>

	@foreach($news as $anews)
	<tr>
		<td>{{$anews->title}}</td>
		<td>{{str_limit($anews->body)}}</td>
		<td>{{$anews->url}}</td>
		<td class="actions">
			<a href="{{route('admin.contents.edit', array($anews->id))}}"><span class="glyphicon glyphicon-edit"></span></a>
			{{ Form::open(array('route' => array('admin.contents.destroy', $anews->id), 'method' => 'delete', 'class' => 'destroy')) }}
            {{ Form::submit('X' , array('class' => "glyphicon glyphicon-remove")) }}
            {{ Form::close() }}
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
			
		</div> {{-- end col md 9 --}}
	</div>{{-- end row --}}

	<div>{{ $news->links() }}</div>
</div>{{-- end containing of content --}}
@stop