<div class="form-group">
	{{ Form::label('title', 'Title') }}
    {{ Form::text('title', null ,array('class' => 'form-control', 'placeholder' => 'Title') ) }}
</div>
<div class="form-group">
	{{ Form::label('body', 'Body') }}
    {{ Form::textarea('body' , null ,array('class' => 'form-control', 'placeholder' => 'Body') ) }}
</div>
<div class="form-group">
	{{ Form::label('url', 'Url') }}
    {{ Form::text('url' , null ,array('class' => 'form-control', 'placeholder' => 'Url') ) }}
</div>
<div class="form-group">
	{{ Form::label('type', 'Type') }}
	{{ Form::select('type', array( 1=>'News', 2=>'Tips')) }}
</div>

<div class="form-group">
	 {{ Form::submit('Save' , array('class' => 'btn btn-default')) }}
</div>
