@extends('_layouts.default')

@section('content')
<div class="users form customFormForLogin">

<div>
     @if(Session::get('errors'))
            <div class="alert alert-danger alert-dismissable">
                 @foreach($errors->all('<p>:message</p>') as $message)
                    {{$message}}
                 @endforeach
            </div>
        @endif
</div>
<h2>Please login</h2>
{{ Form::open(array('route' => 'admin.login.post')) }}
<img class="img-thumbnail fitness_logo" src="{{URL::to('/')}}/img/logo-circle.png" />

    <fieldset>
        <legend>
            Administrator Login
        </legend>
        {{ Form::label('username', 'Email') }}
        {{ Form::email('username') }}<br/>
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password') }}
        {{ Form::submit('Log in', array(
        'class' => 'btn btn-default custom-btn',
        )) }}
        <br/>
        {{ Form::close() }}
    </fieldset>

</div>

<script type="text/javascript">
    $('ul.nav').hide();
</script>
@stop

