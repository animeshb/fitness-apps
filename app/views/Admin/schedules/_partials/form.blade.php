<div class="form-group">
	<div id="result"></div>
	<input type="checkbox" checked="checked" id="reshceduleall" value="0">  Reschedule Only Date 
	<span id="checkbox_value"></span>
</div>
<div class="form-group">
	{{ Form::label('prev_date', 'Prev Date') }}
    {{ Form::text('prev_date', null ,array( 'id' => 'datepicker1', 'class' => 'form-control', 'placeholder' => 'Prev Date') ) }}

</div>
<div class="form-group reschedule_time">
	{{ Form::label('prev_time', 'Prev Time') }}
    {{ Form::text('prev_time', null ,array( 'id' => 'timepicker1', 'class' => 'form-control', 'placeholder' => 'Prev Time') ) }}

</div>

<div class="form-group">
	{{ Form::label('schedule_date', 'New Date') }}
    {{ Form::text('schedule_date' , null ,array( 'id' => 'datepicker', 'class' => 'form-control', 'placeholder' => 'New Date') ) }}
</div>
<div class="form-group previous_time">
	{{ Form::label('schedule_time', 'Schedule Time') }}
    {{ Form::text('schedule_time', null ,array( 'id' => 'timepicker', 'class' => 'form-control', 'placeholder' => 'Schedule Time') ) }}
</div>
<div class="form-group">
	{{ Form::label('place', 'Place') }}
    {{ Form::text('place' , null ,array('class' => 'form-control', 'placeholder' => 'Place') ) }}
</div>
<div class="form-group">
	<label>Notification</label>
	<input type="radio" name="" id="option1" class="radio-button"> Send All
	<input type="radio" name="" id="option2" class="radio-button"> Send Selected
	<div id="user_section">
		{{ Form::label('user_id', 'User List') }}
		{{ Form::select('user_id', User::lists('username', 'id'), 'Choose User' , array('id' => 'myselect')) }}
    	{{ Form::button('adduser', array('id' => 'addUser','value'=>'Add User')) }}
	</div>
	<div id="resultDiv1"></div>
	<div id="resultDiv2"></div>
</div>
<div class="form-group">
	 {{ Form::submit('Save' , array('class' => 'btn btn-default')) }}
</div>
