@extends('_layouts.default')

@section('content')

<div class="users index">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Reschedule List</h1>
			</div>
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="{{route('admin.schedules.create')}}">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp; New Scedule </a>
								</li>
							</ul>
						</div>{{-- end body --}}
				</div>{{-- end panel --}}
			</div>{{-- end actions --}}
		</div>{{-- end col md 3 --}}

		<div class="col-md-9">
	<table cellpadding="0" cellspacing="0" class="table table-striped">
	<thead>
		<tr>
			<th>Prev Schedule</th>
			<th>New Schedule</th>
			<th class="actions">Actions</th>
		</tr>
	</thead>
	<tbody>

	@foreach($schedules as $schedule)
	<tr>
		<td>
			{{$schedule->prev_date}}</br>
			{{$schedule->prev_time}}</br>
		</td>
		<td>
			{{$schedule->schedule_date}}</br>
			{{$schedule->schedule_time}}</br>
			{{$schedule->place}}</br>
		</td>
		<td class="actions">
			{{ Form::open(array('route' => array('admin.schedules.destroy', $schedule->id), 'method' => 'delete', 'class' => 'destroy')) }}
            {{ Form::submit('X' , array('class' => "glyphicon glyphicon-remove")) }}
            {{ Form::close() }}
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
			
		</div> {{-- end col md 9 --}}
	</div>{{-- end row --}}

	<div>{{ $schedules->links() }}</div>
</div>{{-- end containing of content --}}
@stop