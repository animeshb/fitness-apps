@extends('_layouts.default')

@section('content')
<div class="users form">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Add Assessment</h1>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">

								<li>
									<a href="{{route('admin.getAssessList')}}">
										<span class="glyphicon glyphicon-list"></span>&nbsp;&nbsp;List Assessment</a>
								</li>					
							</ul>
						</div>
					</div>
				</div>			
		</div>{{-- end col md 3 --}}
		<div class="col-md-9">
			<div>
				 @if(Session::get('errors'))
                        <div class="alert alert-danger alert-dismissable">
                             @foreach($errors->all('<p>:message</p>') as $message)
                                {{$message}}
                             @endforeach
                        </div>
                    @endif
			</div>


			{{ Form::open(array('route' => 'admin.postNewAsess' , "onsubmit" => "return validateForm()", 
					  "id" => "setAssessmentUser")) }}

				@if( isset($tabinfo) )

					{{Form::hidden('old_date',$assess_date)}}
					{{Form::hidden('old_time',$assess_time)}}
					
					 @include('admin.users._partials.assessform',
					 	array('tabinfo'=>$tabinfo,'assess_time'=>$assess_time,
					 			'assess_date'=>$assess_date,'userlist' =>$userlist
					 	))

				@else
					 @include('admin.users._partials.assessform')
				@endif
				
			{{ Form::close() }}
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}
</div>
<script>
  $(function() {
  	$('#datepicker').datepicker();
  	$('#timepicker').timepicker({minuteGrid: 15});

  	$('#forAssessmentSave').hide();

  	$( "#addUser" ).click(function(event) {
		event.preventDefault();
		//alert('hi');
		submitForm("#setUserForAssessment");
	});


$(document).on('click', '.remove-td', function(event){

		$(this).parent().parent().remove();

		var item = $(this).data( "role" );

		var index = userArray.indexOf(item);

		if (index > -1) {
		    userArray.splice(index, 1);
		}

});
	

	$(document).on('click', '#setuser', function(event){ 

		

		//event.preventDefault();
		var userId = $(this).prev().val();
		//alert(userId); die();
		if(userId){
			$('#result').hide();	
			var getaddress_url = adminURL + "users/addassessmentperuser";

            $.post( getaddress_url , { userId : userId } ,callbackassessment);
		}
		else{
		  $('#result').html('Please add user to continue!');
		  $('#result').addClass('err');
		  return false;
		}
		

	});

  });
  <?php 

  	  if(isset($userlist)){
  	  	echo 'var userArray = ['.implode(',', array_keys($userlist) ).'];';
  	  } else {
  	  		echo 'var userArray = new Array();';
  	  }

   ?>
 
  function callbackassessment(data){

  				var item = data;
				var len = item.assessment.length;
				var users_data = item.user;
				var assessment_data =item.assessment;
				var assessment_unit_data = [];
                var txt = "";
                var txt1 = "";
                var txt2 = "";
                var username = users_data.username.split("@");
                username = username[0];
                var userid = users_data.id;

                if( jQuery.inArray(userid, userArray)===-1 && len){

                	userArray.push(userid);

                		txt1 += '<tr><td>'+username+'</td>';
                		
                    		for(var i=0;i<len;i++){	

	                    			
	                    			txt2 += '<td><input type="text" class="assessmentList" name="data[A-'+assessment_data[i].id+'][U-'+userid+']" placeholder="No..." />'+assessment_data[i].unit+'</td>'; 
	                    		}
	                  
	                  		txt2 += "<td><button class='remove-td' data-role='"+userid+"'>X</button></td>";	
	                    	txt+= txt1+txt2;
	                  
                    	if(txt != ""){
                        	$("#assessmentTable").append(txt);
                    	}
                
                    }
                    //$('#forAssessmentEdit').hide();
                    $('#forAssessmentSave').show();
	}

	// form validation added
	function validateForm()
	{
	var x=$('#datepicker').val();
	var y=$('#timepicker').val();
	if (x=="" || y=="")
	  {
	  	$('#result').show();
	  $('#result').html('Please fill date and time to add assessment!');
	  $('#result').addClass('err');
	  return false;
	  }
	}
 </script>
@stop
