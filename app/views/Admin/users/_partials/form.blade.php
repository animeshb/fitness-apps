<div id="myTabContent" class="tab-content">
<div class="tab-pane fade active in" id="profile">
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->first_name:null }}
		{{ Form::label('first_name', 'First Name') }}
	    {{ Form::text('profile[first_name]',$val , 
	    	array('class' => 'form-control', 'placeholder' => 'First Name') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->last_name:null }}
		{{ Form::label('last_name', 'Last Name') }}
	    {{ Form::text('profile[last_name]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Last Name') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->regId:null }}
		{{ Form::label('regId', 'Device Id') }}
	    {{ Form::text('profile[regId]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Device Id') ) }}
	</div>

	<div class="form-group">
		{{''; $val1 = isset($profile)? $profile->gender:null }}
		{{ Form::label('gender', 'Gender') }}
		{{'';$val = array('male'=>'Male','female'=>'Female')}}
		{{Form::select('profile[gender]', $val,$val1,array('class' => 'form-control', 'placeholder' => 'Gender'))}}
	</div>

	<div class="form-group">
		{{''; $val = isset($profile)? $profile->birth_date:null }}
		{{ Form::label('', 'Birth Date') }}
	    {{ Form::text('profile[birth_date]', $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Birth Date') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->address1:null }}
		{{ Form::label('', 'Address1') }}
	    {{ Form::text('profile[address1]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Address1') ) }}
	</div>

	<div class="form-group">
		{{''; $val = isset($profile)? $profile->phone:null }}
		{{ Form::label('', 'Phone') }}
	    {{ Form::text('profile[phone]', $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Phone') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->mobile:null }}
		{{ Form::label('', 'Mobile') }}
	    {{ Form::text('profile[mobile]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Mobile') ) }}
	</div>

	<div class="form-group">
		{{''; $val = isset($profile)? $profile->address2:null }}
		{{ Form::label('', 'Address2') }}
	    {{ Form::text('profile[address2]', $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Address2') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->city:null }}
		{{ Form::label('', 'City') }}
	    {{ Form::text('profile[city]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'City') ) }}
	</div>

	<div class="form-group">
		{{''; $val = isset($profile)? $profile->postcode:null }}
		{{ Form::label('', 'Postcode') }}
	    {{ Form::text('profile[postcode]', $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Postcode') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->state:null }}
		{{ Form::label('', 'State') }}
	    {{ Form::text('profile[state]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'State') ) }}
	</div>
	<div class="form-group">
		{{''; $val = isset($profile)? $profile->country:null }}
		{{ Form::label('', 'Country') }}
	    {{ Form::text('profile[country]' , $val , 
	    	array('class' => 'form-control', 'placeholder' => 'Country') ) }}
	</div>

	
</div>
<div class="tab-pane fade" id="medicalinfo">
	<div class="form-group">
	@foreach(DB::select('select id,name from medicals') as $message)
		@if( in_array($message->id,$medi_id) )
		
			{{ Form::checkbox('medical[]', $message->id, true, array('class' => 'name')) }}
		@else
			{{ Form::checkbox('medical[]', $message->id, false, array('class' => 'name')) }}

		@endif
		{{$message->name}}	
		<br />
	@endforeach
	</div>
	<div class="form-group">
		<div class="input text"><label for="others">Others</label>
		<input type="text" id="others" maxlength="50" placeholder="others..." class="form-control" name="MedicalsUser">
		</div>				
	</div>
	<div class="form-group">
		 	{{ Form::submit('Save' , array('class' => 'btn btn-default')) }}
	</div>
</div>	
</div>
