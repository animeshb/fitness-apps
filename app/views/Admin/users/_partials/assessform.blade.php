<div id="result"></div>
<div id="userAssessment"></div>
<div class="form-group">
	{{''; $val = isset($assess_date)? $assess_date:null }}
	{{ Form::label('assessment_date', 'Date') }}
    {{ Form::text('assessment_date', $val ,array(  'id' => 'datepicker', 'class' => 'form-control', 'placeholder' => 'Date') ) }}

</div>
<div class="form-group">
	{{''; $val = isset($assess_date)? $assess_date:null }}
	{{ Form::label('assessment_time', 'Time') }}
    {{ Form::text('assessment_time', $val ,array(  'id' => 'timepicker', 'class' => 'form-control', 'placeholder' => 'Time') ) }}

</div>
<div class="form-group">
		{{ Form::label('user_id', 'User List') }}
		{{ Form::select('user_id', User::lists('username', 'id') ) }}
    	{{ Form::button('adduser', array('id' => 'setuser','name' => 'setuser','value'=>'Add User')) }}
</div>

<div class="form-group">
	<div class="table-responsive">
		<table id="assessmentTable" class="table table-bordered table-striped table-condensed">
		<tbody>
			<tr>
				<th>Members</th>
				 {{''; $assesslist =[] }}
				 @foreach(DB::select('select id,unit,name from assessments') as $message)
				{{''; $assesslist[$message->id] = $message->unit }}
                    {{'<th>'.$message->name.'</th>'}}
                 @endforeach
                 <th>DEL</th>
			</tr>
			@if( isset($tabinfo) )

			{{'';$newrow = 0 }}

			{{'';$count = 0 }}

			@foreach($tabinfo as $value)

				@if( $newrow != $value->user_id )
					{{''; $newrow = $value->user_id }}
					<tr>
						<td>{{$userlist[$value->user_id]}}</td>
				@endif

				{{'';$count++ }}
				
				<td><input type="text" class="assessmentList" name="{{'data[A-'.$value->assessment_id.'][U-'.$value->user_id.']'}}" 
						value="{{ $value->assessment}}" />{{ $assesslist[$value->assessment_id]}}</td>		

				@if( $count == count($assesslist) )

				{{'';$count = 0 }}
				<td><button class='remove-td' data-role="{{$value->user_id}}">X</button></td>

					</tr>
				@endif
			@endforeach

			@endif
		</tbody>
		</table>
</div>
@if( isset($tabinfo) )
<div id="forAssessmentEdit" class="submit">
@else
<div id="forAssessmentSave" class="submit">
@endif
	 {{ Form::submit('Save' , array('class' => 'btn btn-default')) }}
</div>
