<div class="form-group">
	{{ Form::label('username', 'UserName') }}
    {{ Form::text('username', null ,array('class' => 'form-control', 'placeholder' => 'Username') ) }}

</div>
<div class="form-group">
	{{ Form::label('password', 'Password') }}
    {{ Form::password('password' , array('class' => 'form-control', 'placeholder' => 'Password') ) }}
</div>
<div class="form-group">
	{{ Form::label('token', 'Token') }}
    {{ Form::text('token' , null ,array('class' => 'form-control', 'placeholder' => 'Token') ) }}
</div>

<div class="form-group">
	 {{ Form::submit('Save' , array('class' => 'btn btn-default')) }}
</div>
