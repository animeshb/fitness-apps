@extends('_layouts.default')

@section('content')

<div class="users index">

	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>Users Assessment</h1>
			</div>
		</div>{{-- end col md 12 --}}
	</div>{{-- end row --}}

	<div class="row">
		<div class="col-md-3">
			<div class="actions">
				<div class="panel panel-default">
					<div class="panel-heading">Actions</div>
						<div class="panel-body">
							<ul class="nav nav-pills nav-stacked">
								<li>
									<a href="{{route('admin.getNewAsess')}}">
										<span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;New Assessments</a>
								</li>
							</ul>
						</div>{{-- end body --}}
				</div>{{-- end panel --}}
			</div>{{-- end actions --}}
		</div>{{-- end col md 3 --}}

		<div class="col-md-9">
			<table cellpadding="0" cellspacing="0" class="table table-striped">
				<thead>
					<tr>
			<th>Assessment Date</th>
			<th>Time</th>
			<th class="actions">Actions</th>
	</tr>
	</thead>
	<tbody>
	@foreach($result as $value)
	<tr>
		<td>{{$value->assessment_date}}</td>
		<td>{{substr($value->assessment_time, 0, 5)}}</td>
		<td class="actions">

			{{ Form::open(array('route' => array('admin.editAsess'), 'method' => 'post', 'class' => 'destroy')) }}
            {{Form::hidden('assess_date',$value->assessment_date);}}
            {{Form::hidden('assess_time',$value->assessment_time);}}
            {{ Form::submit('EDIT' , array('class' => "glyphicon glyphicon-remove")) }}
            {{ Form::close() }} |
            {{ Form::open(array('route' => array('admin.delAsess'), 'method' => 'post', 'class' => 'destroy')) }}
            {{Form::hidden('assess_date',$value->assessment_date);}}
            {{Form::hidden('assess_time',$value->assessment_time);}}
	        {{ Form::submit('DEL' , array('class' => "glyphicon glyphicon-remove")) }}
            {{ Form::close() }}
            
		</td>
	</tr>
	@endforeach
	</tbody>
</table>
		
		</div> {{-- end col md 9 --}}
	</div>{{-- end row --}}

</div>{{-- end containing of content --}}
@stop