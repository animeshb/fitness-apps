<!DOCTYPE html>
<html lang="en">
  <head>
	<title>
    Ftiness
	</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

	

  	<!-- Latest compiled and minified CSS -->
  	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

  	<!-- Latest compiled and minified JavaScript -->
  	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  	<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    {{ HTML::style('css/calendar.css') }}
    {{ HTML::style('css/mycustom.css') }}
    {{ HTML::style('css/jquery-ui-timepicker-addon.css') }}

    {{ HTML::style('css/font-awesome.css') }}
    {{ HTML::style('css/jquery-ui.css') }}
    {{ HTML::style('css/jquery-ui-timepicker-addon.css') }}
      
    {{ HTML::script('js/jquery-1.9.1.js') }}
    {{ HTML::script('js/jquery-ui.js') }}
    {{ HTML::script('js/calendar.js') }}
    {{ HTML::script('js/ajax.js') }}
    {{ HTML::script('js/services.js') }}
    {{ HTML::script('js/jquery-ui-sliderAccess.js') }}
    {{ HTML::script('js/jquery-ui-timepicker-addon.js') }}

    

<style type="text/css">
	body{ padding: 70px 0px; }
</style>

<script type="text/javascript">
    function init() {
      calendar.set("birthday");
      calendar.set("anotherday");
    }
</script>

<script type="text/javascript">
            $(function(){
        function stripTrailingSlash(str) {
          if(str.substr(-1) == '/') {
            return str.substr(0, str.length - 1);
          }
          return str;
        }

        var url = window.location.pathname;  
        var activePage = stripTrailingSlash(url);

        $('.nav li a').each(function(){  
          var currentPage = stripTrailingSlash($(this).attr('href'));

          if (activePage == currentPage) {
            $(this).parent().addClass('active'); 
          } 
        });
      });
</script>

  <head>
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Fitness Express OZ</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Home</a></li>
            <li><a href="{{route('admin.users.index')}}">Users List</a></li>
            <li><a href="{{route('admin.getAssessList')}}">Users Assessment</a></li>
            <li><a href="{{route('admin.schedules.index')}}">Reschedule</a></li>
            <li><a href="{{route('admin.news')}}">News</a></li>
            <li><a href="{{route('admin.tips')}}">Tips</a></li>
            <li><a href="{{route('admin.medicals.index')}}">Medicals</a></li>
            <li><a href="{{route('admin.assessments.index')}}">Assessments</a></li>
            <li><a href="{{route('admin.logout')}}">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div class="container">
			 @yield('content')
    </div>
  </body>
</html>
