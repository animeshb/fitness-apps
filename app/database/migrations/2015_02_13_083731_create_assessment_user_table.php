<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssessmentUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('assessment_user', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('assessment_id')->unsigned()->index();
			$table->foreign('assessment_id')->references('id')->on('assessments')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->integer('assessment')->unsigned();
			$table->date('assessment_date');
			$table->time('assessment_time');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('assessment_user');
	}

}
