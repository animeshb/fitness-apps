<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AssessmentUserTableSeeder extends Seeder {

	public function run()
	{
		$role_user = array(
		    array('assessment_id' => 5, 'user_id'=>5, 'assessment'=>5,  'created_at' => new DateTime, 'updated_at' => new DateTime,),
		    array('assessment_id' => 6, 'user_id'=>6, 'assessment'=>10, 'created_at' => new DateTime, 'updated_at' => new DateTime,),
			
			array('assessment_id' => 6, 'user_id'=>5, 'assessment'=>5,  'created_at' => new DateTime, 'updated_at' => new DateTime,),
		    array('assessment_id' => 5, 'user_id'=>6, 'assessment'=>10, 'created_at' => new DateTime, 'updated_at' => new DateTime,)

		);

		// Uncomment the below to run the seeder
		DB::table('assessment_user')->truncate();
		DB::table('assessment_user')->insert($role_user);


	}

}