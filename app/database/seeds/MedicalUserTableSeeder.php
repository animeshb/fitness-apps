<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class MedicalUserTableSeeder extends Seeder {

	public function run()
	{
		$role_user = array(
		    array('medical_id' => 4, 'user_id'=>1,  'created_at' => new DateTime, 'updated_at' => new DateTime,),
		    array('medical_id' => 4, 'user_id'=>2, 'created_at' => new DateTime, 'updated_at' => new DateTime,),
			
			array('medical_id' => 6, 'user_id'=>2,  'created_at' => new DateTime, 'updated_at' => new DateTime,),
		    array('medical_id' => 6, 'user_id'=>1, 'created_at' => new DateTime, 'updated_at' => new DateTime,)

		);

		// Uncomment the below to run the seeder
		DB::table('medical_user')->truncate();
		DB::table('medical_user')->insert($role_user);


	}

}