<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class SchedulesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 5) as $index)
		{
			Schedule::create([

				'prev_date' => $faker->date(),
				'prev_time' => $faker->time(),
				'schedule_date' => $faker->date(),
				'schedule_time' => $faker->time(),
				'place' => $faker->randomElement(['ctg','dhk']),
				'sent_to' => 'all'
			]);
		}
	}

}