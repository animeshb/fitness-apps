<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
		$this->call('ContentsTableSeeder');
		$this->call('MedicalsTableSeeder');
		$this->call('SchedulesTableSeeder');
		$this->call('MedicalUserTableSeeder');
		$this->call('AssessmentsTableSeeder');
		$this->call('AssessmentUserTableSeeder');
		
	}

}
