<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AssessmentsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Assessment::create([

				'name' => $faker->word,
                'unit' => $faker->numberBetween(1,10)

			]);
		}
	}

}