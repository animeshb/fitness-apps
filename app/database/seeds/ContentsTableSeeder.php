<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ContentsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
			Content::create([

				'title' => $faker->text(20),
				'body' => $faker->text(50),
				'url' => $faker->url(),
				'type' => $faker->randomElement([1,2])

			]);
		}
	}

}