<?php

class AdminMedicalsController extends \BaseController {

	/**
	 * Display a listing of medicals
	 *
	 * @return Response
	 */
	public function index()
	{
		$medicals = Medical::allPaginate();

		return View::make('admin.medicals.index', compact('medicals'));
	}

	/**
	 * Show the form for creating a new medical
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.medicals.create');
	}

	/**
	 * Store a newly created medical in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Medical::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Medical::create($data);

		return Redirect::route('admin.medicals.index');
	}

	/**
	 * Display the specified medical.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$medical = Medical::findOrFail($id);

		return View::make('admin.medicals.show', compact('medical'));
	}

	/**
	 * Show the form for editing the specified medical.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$medical = Medical::find($id);

		return View::make('admin.medicals.edit', compact('medical'));
	}

	/**
	 * Update the specified medical in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$medical = Medical::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Medical::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$medical->update($data);

		return Redirect::route('admin.medicals.index');
	}

	/**
	 * Remove the specified medical from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Medical::destroy($id);

		return Redirect::route('admin.medicals.index');
	}

}
