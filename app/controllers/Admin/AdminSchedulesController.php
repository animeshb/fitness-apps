<?php

class AdminSchedulesController extends \BaseController {

	/**
	 * Display a listing of schedules
	 *
	 * @return Response
	 */
	public function index()
	{
		$schedules = Schedule::allPaginate();

		return View::make('admin.schedules.index', compact('schedules'));
	}

	/**
	 * Show the form for creating a new schedule
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.schedules.create');
	}

	/**
	 * Store a newly created schedule in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		$data = Input::all();


	

        
		$validator = Validator::make($data, Schedule::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}
		


			$toDevice = array();
			
			if($data['wholeday'] == 1){
				unset($data['prev_time'], $data['schedule_time'], $data['wholeday']);
			}
			else{
				unset($data['wholeday']);
			}

			if(isset($data['sent_to'])){
				
				$data['sent_to'] = User::whereIn('username', $data['sent_to'] )->select(['username'])->get();
				
				$names = [];
				foreach ($data['sent_to'] as $key => $value) {

					//push notification regid 
					//is in the profile table now ...
					
					//if($value->regId !== '' || $value->regId !== null){

						//array_push($toDevice, $value->regId);

						$names[] = $value->username;

					//}
						
			
				}

				$data['sent_to'] = json_encode($names);
				      

			}
			else if(isset($data['sentall'])){

				$data['sent_to'] = $data['sentall'];

				unset($data['sentall']);


				$allUsers = User::lists('regId');

			   foreach ($allUsers as $regId) {
				   	if($regId !== '' || $regId !== null)
						array_push($toDevice, $regId);
				}
			}

			$data['schedule_date'] = $this->date_convert($data['schedule_date']);
			$data['prev_date'] = $this->date_convert($data['prev_date']);

			// echo '<pre>';
	  //       var_dump( $data );
	  //       echo '</pre>';
	  //       die();


			
			if (Schedule::create($data)) {

					//$apiKey = "AIzaSyBpxhbsJuvN70EZ4Gxn8o2u5MFzjgHT3UI";
					
					
					// $msg = 'Reschedule';
					// $pushInfo = array('apiKey'=>$apiKey,'target'=>$toDevice, 'msg' => $msg );
					
					// $sendResult = $this->sendPushnotification($pushInfo);

			} else {


				return Redirect::back()->withErrors(['Schedule Not Saved'])->withInput();
			}

		

		return Redirect::route('admin.schedules.index');
	}


	public function sendPushnotification($pushInfo){
		$message = $pushInfo['msg'];
		// Set POST variables
		$url = 'https://android.googleapis.com/gcm/send';

		$fields = array(
						'registration_ids'  => $pushInfo['target'],
						'data'              => array( "message" => $message ),
						'collapse_key' 		=> 'demo',
						);
		$headers = array( 
						'Authorization: key=' .$pushInfo['apiKey'],
						'Content-Type: application/json'
						);

		// Open connection
		$ch = curl_init();

		// Set the url, number of POST vars, POST data
		curl_setopt( $ch, CURLOPT_URL, $url );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
		// Execute post
		$result = curl_exec($ch);
		// Close connection
		curl_close($ch);
		
	}

	/**
	 * Display the specified schedule.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$schedule = Schedule::findOrFail($id);

		return View::make('admin.schedules.show', compact('schedule'));
	}

	/**
	 * Show the form for editing the specified schedule.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$schedule = Schedule::find($id);

		return View::make('admin.schedules.edit', compact('schedule'));
	}

	/**
	 * Update the specified schedule in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$schedule = Schedule::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Schedule::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$schedule->update($data);

		return Redirect::route('admin.schedules.index');
	}

	/**
	 * Remove the specified schedule from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Schedule::destroy($id);

		return Redirect::route('admin.schedules.index');
	}

	public function date_convert($date_str){

		if(empty($date_str)) return '';
		$old_date = date($date_str);
		$old_date_timestamp = strtotime($old_date);
		return date('Y-m-d', $old_date_timestamp);
	}

}
