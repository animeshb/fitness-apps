<?php

class AdminAssessmentsController extends \BaseController {

	/**
	 * Display a listing of assessments
	 *
	 * @return Response
	 */
	public function index()
	{
		$assessments = Assessment::allPaginate();

		return View::make('admin.assessments.index', compact('assessments'));
	}

	/**
	 * Show the form for creating a new assessment
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.assessments.create');
	}

	/**
	 * Store a newly created assessment in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Assessment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Assessment::create($data);

		return Redirect::route('admin.assessments.index');
	}

	/**
	 * Display the specified assessment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$assessment = Assessment::findOrFail($id);

		return View::make('admin.assessments.show', compact('assessment'));
	}

	/**
	 * Show the form for editing the specified assessment.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$assessment = Assessment::find($id);

		return View::make('admin.assessments.edit', compact('assessment'));
	}

	/**
	 * Update the specified assessment in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$assessment = Assessment::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Assessment::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$assessment->update($data);

		return Redirect::route('admin.assessments.index');
	}

	/**
	 * Remove the specified assessment from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Assessment::destroy($id);

		return Redirect::route('admin.assessments.index');
	}

}
