<?php

class AdminUsersController extends \BaseController {

	/**
	 * Display a listing of users
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::allPaginate();

		return View::make('admin.users.index', compact('users'));
	}




	/**
	 * Display a listing of users
	 *
	 * @return Response
	 */
	public function getAssessList()
	{
	
		$result = DB::select(' select DISTINCT(assessment_date), assessment_time from assessment_user ');
		       
		return View::make('admin.users.assessmentslist', compact('result'));

	}


	public function getNewAsess()
	{
		// $users = User::allPaginate();

		//dd($assesslist = Assessment::lists('name'));exit;

		return View::make('admin.users.adduserassesments');
	}

	public function postNewAsess()
	{
		$data = Input::all();

		if(isset($data['old_date'])) {

			// DB::delete('delete from assessment_user where assessment_date = ? and assessment_time = ?', 
			// array( $data['old_date'], $data['old_time'] ));

			DB::table('assessment_user')
	        ->where('assessment_date', $data['old_date'])
	        ->where('assessment_time', $data['old_time'])
	        ->delete();

			unset($data['old_date']);
			unset($data['old_time']);

		}
		       
	    $assessmentDate = $this->date_convert($data['assessment_date']);
		$assessmentTime = $data['assessment_time'];
		
        $tobeSaved = array();

        $count = 0;
        foreach ($data['data'] as $key => $value) {

            $aid = $this->getId($key);

            //echo 'assess_id '.$aid; 

            
            foreach ($value as $key1 => $value1) {

                $uid = $this->getId($key1);

                 //echo 'user_id '.$uid; 

                $tobeSaved[$count]['user_id'] = $uid;
                $tobeSaved[$count]['assessment_id'] = $aid;
                $tobeSaved[$count]['assessment_date']=$assessmentDate;
                $tobeSaved[$count]['assessment_time']=$assessmentTime;
                if($value1 == NULL){
                	$tobeSaved[$count]['assessment']=0;
                }
                else{
                	$tobeSaved[$count]['assessment']=$value1;
                }
                
            
            	$count++;       
          
            }

        }


        DB::table('assessment_user')->insert($tobeSaved);

        return $this->getAssessList();
		
	}



	public function geteditAsessbyDate(){

	
	}

	public function posteditAsessbyDate(){

		$data = Input::all();


		$result = DB::select('select *  from assessment_user   where assessment_date = ? and assessment_time = ? order by  user_id asc, assessment_id asc', 
					array( $data['assess_date'], $data['assess_time'] ));

		$uids = [];

		foreach ($result as $key => $value) {

			$uids[] = $value->user_id;
		}

		$uids = array_unique($uids);

		$userlist = DB::table('users')->whereIn('id', $uids)->select('username','id')->get();

		$temp = [];
		foreach ($userlist as $key => $value) {
			//$temp[$value->id] = array_pop(explode('@',$value->username));

			$temp[$value->id] = $value->username;
		}

		$userlist = $temp;

		        // echo '<pre>';
		   

		return View::make('admin.users.adduserassesments',
			array('userlist'=>$userlist,
				'tabinfo' => $result,
				//'uidlist'=>$uids,
				//'assesslist'=>$assesslist,
				'assess_date' => $data['assess_date'], 
				'assess_time' => $data['assess_time']
				));
		     

	}

	public function delAsessbyDate(){

		$data = Input::all();

		DB::delete('delete from assessment_user where assessment_date = ? and assessment_time = ?', 
					array( $data['assess_date'], $data['assess_time'] ));

		return $this->getAssessList();

		      
		
	}

	public function addassessmentperuser(){

				$data = Input::all();

				$userInfo = User::findOrfail($data['userId']);
				$assessmentInfo = Assessment::all();
				$response = array('user'=>$userInfo, 'assessment' =>$assessmentInfo);
				     
				return Response::json($response);

	}



	/**
	 * Show the form for creating a new user
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.users.create');
	}

	/**
	 * Store a newly created user in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

		
		$validator = Validator::make($data = Input::all(), User::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$data = Input::all();
		$data['password'] = Hash::make($data['password']);
		User::create($data);

		return Redirect::route('admin.users.index');
	}

	/**
	 * Display the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);

		return View::make('admin.users.show', compact('user'));
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::with('Medical','Profile')->findOrFail($id);

		$medi_id = $user->medical()->getRelatedIds();
		        
		return View::make('admin.users.edit', array('user' => $user, 'medi_id' => $medi_id ));
	}

	/**
	 * Update the specified user in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		$data = Input::all();


		$validator = Validator::make($data['profile'] , Profile::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		/* saving medical assessment info */
		

		if( !empty($data['MedicalsUser'])){

			$new_medi_id = Medical::create( array('name' => $data['MedicalsUser']) )->id;

			$data['medical'][] = $new_medi_id;
		}

		if( !isset( $data['medical'] ) ){

			$data['medical'] = [];
		} 
		      
		$user = User::with('Medical','Profile')->findOrFail($id);

		$user->medical()->sync( $data['medical'] );

		/* saving profile info */

		      
		$data['profile']['user_id'] = $id;
		$feature = new Profile();
		$feature->unguard();
		$feature->fill($data['profile']);
		$feature->exists = Input::has('profile.id');
		$feature->reguard();
		$feature->save();

		

		

		return Redirect::route('admin.users.index');
	}

	/**
	 * Remove the specified user from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);

		return Redirect::route('admin.users.index');
	}


	public function date_convert($date_str){

		if(empty($date_str)) return '';
		$old_date = date($date_str);
		$old_date_timestamp = strtotime($old_date);
		return date('Y-m-d', $old_date_timestamp);
	}

	public function getId($key){

		$setid = explode('-', $key);
		//pr($setid); exit;
		return $setid[1];
	}

}
