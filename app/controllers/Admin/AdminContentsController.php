<?php

class AdminContentsController extends \BaseController {


	public function getNews()
	{
		//$contents = Content::all();

		$news = Content::newsPaginate();

		$type = 1;

		return View::make('admin.contents.index', ['type' => $type,'news' => $news]);
	}

	public function getTips()
	{
		//$contents = Content::all();

		$news = Content::newsPaginate(2);

		$type = 2;

		return View::make('admin.contents.index', ['type' => $type,'news' => $news]);
	}

	/**
	 * Display a listing of contents
	 *
	 * @return Response
	 */
	public function index()
	{
		$contents = Content::all();

		return View::make('contents.index', compact('contents'));
	}

	/**
	 * Show the form for creating a new content
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.contents.create');
	}

	/**
	 * Store a newly created content in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Content::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Content::create($data);

		if($data['type']==1){

			return Redirect::route('admin.news');
		} else {

			return Redirect::route('admin.tips');
		}

		
	}

	/**
	 * Display the specified content.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$content = Content::findOrFail($id);

		return View::make('contents.show', compact('content'));
	}

	/**
	 * Show the form for editing the specified content.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$content = Content::find($id);

		return View::make('admin.contents.edit', compact('content'));
	}

	/**
	 * Update the specified content in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$content = Content::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Content::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$content->update($data);

		if($data['type']==1){

			return Redirect::route('admin.news');
		} else {

			return Redirect::route('admin.tips');
		}
	}

	/**
	 * Remove the specified content from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		$content = Content::findOrFail($id);
		Content::destroy($id);

		if($content->type==1){

			return Redirect::route('admin.news');
		} else {

			return Redirect::route('admin.tips');
		}

	}

}
