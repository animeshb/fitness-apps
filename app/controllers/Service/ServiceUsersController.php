<?php

class ServiceUsersController extends \BaseController {


	public function assessmentcurvedata($assessmentId,$userId){

		$data = Input::all();

		$response = DB::table('assessment_user')
					->where('user_id', '=', $userId)
					->where('assessment_id', '=', $assessmentId)
					->orderBy('assessment_date' , 'ASC')
					->get(); 
		$data = array('success' => true, 'message' => 'Assessment User result', 'data' => array('data' => $response));
		return Response::json($data);
	

	}

	public function medicallist(){

		$response = Medical::lists('id','name');
			
		$data = array('success' => true, 'message' => 'User profile Info', 
					'data' => array( 'data' => $response  ));

		return Response::json($data);

	}


	public function profilemedical(){





		$id = Auth::User()->id;

		$user = User::with('Medical','Profile')->findOrFail($id);

		$response = [];

		$response['profile'] = $user->profile()->first();

		$response['medical'] = $user->medical()->getRelatedIds();

		$response['medi_list'] = Medical::lists('id','name');

				
		$data = array('success' => true, 'message' => 'User profile Info', 
					'data' => array('data' => $response));


		

		return Response::json($data);
		
	}


	public function updateprofile(){



		$data = Input::all();


		$validator = Validator::make($data['profile'] , Profile::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		/* saving medical assessment info */
		

		if( !empty($data['MedicalsUser'])){

			$new_medi_id = Medical::create( array('name' => $data['MedicalsUser']) )->id;

			$data['medical'][] = $new_medi_id;
		}

		$id = Auth::user()->id;

		        // echo '<pre>';
		        // var_dump( Input::all() );
		        // echo '</pre>';
		        // die();

		if( !isset( $data['medical'] ) ){

			$data['medical'] = [];
		}        
		      
		$user = User::with('Medical','Profile')->findOrFail($id);

		$user->medical()->sync( $data['medical'] );

		/* saving profile info */

		      
		$data['profile']['user_id'] = $id;
		$feature = new Profile();
		$feature->unguard();
		$feature->fill($data['profile']);
		$feature->exists = Input::has('profile.id');
		$feature->reguard();
		$feature->save();

		$data = array('success' => true, 'message' => 'Profile Update', 'data' =>  [] );
		return Response::json($data);

	}


	public function assessmentlist(){
		$response = DB::table('assessments')->get();; 
		$data = array('success' => true, 'message' => 'Assessment result', 'data' => array('data' => $response));
		return Response::json($data);
	}



	public function findtips(){
		$news = DB::table('contents')->where('type', '=', 2)->orderBy('id','desc')->take(5)->get();
		$data = array('success' => true, 'message' => 'Tips result', 'data' =>  $news );
		 return Response::json($data);
	} 


	public function findnews(){
		$news = DB::table('contents')->where('type', '=', 1)->orderBy('id','desc')->take(5)->get();
		$data = array('success' => true, 'message' => 'News result', 'data' =>  $news );
		return Response::json($data);
	} 


	public function newsdetails($news_id){

		//return Response::json(Input::get('news_id'));
		$news = Content::findOrFail( $news_id );
		$data = array('success' => true, 'message' => 'News result', 'data' =>  $news );
		return Response::json($data);
	} 


	

}
