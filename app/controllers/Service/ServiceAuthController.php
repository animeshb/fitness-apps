<?php


class ServiceAuthController extends \BaseController
{



    public function postRegister(){

        $data = Input::all();

        $array = array($data['year'], $data['month'], $data['day']);
        $data['profile']['birth_date'] = implode("-", $array);


        $decision = true;

        $validator = Validator::make($data['user'], User::$rules);
        if ($validator->fails())
        {
            $decision = false;
        }


        $validator = Validator::make($data['profile'], Profile::$rules);
        if ($validator->fails())
        {
            $decision = false;
        }

        if( !$decision  ){


            $data = array('success' => true, 'message' => 'Failed To Register', 'data' =>  [] );

        }


        /* saving a user */ 

        $token = substr( "abcdefghijklmnopqrstuvwxyz", mt_rand(0, 25) , 1) .substr( md5( time() ), 1);

        $info = array(

                'username' => $data['user']['username'],

                'password' => Hash::make( $data['user']['password'] ),

                'token' => $token
            );

        $id = User::create($info)->id;


        /* saving medical assessment info */
        

        if( !empty($data['MedicalsUser'])){

            $new_medi_id = Medical::create( array('name' => $data['MedicalsUser']) )->id;

            $data['medical'][] = $new_medi_id;
        }

       

        if( !isset( $data['medical'] ) ){

            $data['medical'] = [];
        }        
              
        $user = User::with('Medical','Profile')->findOrFail($id);

        $user->medical()->sync( $data['medical'] );

        /* saving profile info */

        $profile = new Profile($data['profile']);

         $user->profile()->save( $profile );
              
        // $data['profile']['user_id'] = $id;
        // $feature = new Profile();
        // $feature->unguard();
        // $feature->fill($data['profile']);
        // $feature->exists = Input::has('profile.id');
        // $feature->reguard();
        // $feature->save();

         if (Auth::attempt(array('username' => $data['user']['username'], 'password' => $data['user']['password'] ) ) ){

                $sample = array('logged' => 'yes', 'token'=>'h3ohn5#3n300mjh3ndmabo9383n3h3');

                $userInfo = User::findOrfail( Auth::user()->id );
                $data = array('success' => true, 'message' => "succesfully logged in", 'data' => $userInfo );
                
        }

        $data = array('success' => true, 'message' => 'Register', 'data' =>  $data );


        return Response::json($data);




    }

    public function getLogin(){
        return View::make('admin.auth.login');
    }

    public function postLogin(){
        $data = Input::all();

        //return Response::json($data);


        $decision = true;

        $validator = Validator::make($data, User::$rules);
        if ($validator->fails())
        {
            $decision = false;
        }

        if (Auth::attempt(array('username' => Input::get('username'), 'password' => Input::get('password') ) ) ){

                $sample = array('logged' => 'yes', 'token'=>'h3ohn5#3n300mjh3ndmabo9383n3h3');

                $userInfo = User::findOrfail( Auth::user()->id );
                $data = array('success' => true, 'message' => "succesfully logged in", 'data' => $userInfo );
                
        } else {

             $decision = false;
        }

        if( !$decision ){

            $data = array('success' => false, 'message' => "Invalid Credintial", 
                            'data' =>  [] );

        }
                     
        return Response::json($data);

    }

    public function getLogout(){
        Auth::logout();

         $data = array('success' => true, 'message' => "logout", 
                            'data' =>  [] );
         return Response::json($data);
    }
} 